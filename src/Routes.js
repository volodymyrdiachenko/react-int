import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from "./pages/Home";
import About from "./pages/About";
import Product from "./pages/Product";
import Products from "./pages/Products";


const Routes = () => (
  <main role="main">
    <Switch>
      <Route path="/" exact component={Home} />
      <Route exact path="/products" component={Products} />
      <Route path="/about" component={About} />
      <Route exact path="/products/:id" component={Product} />
    </Switch>
  </main>
);

export default Routes;
