import { combineReducers } from "redux";
import { product } from "../../pages/Product/ProductPage.reducers";

const rootReducer = combineReducers({
    product
});

export default rootReducer;