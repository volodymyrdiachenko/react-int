import React from "react";
import StyledContainer from "../../shared/components/Container";
import Hero from "../../shared/components/Hero";
import ProductList from '../../shared/components/ProductsList';
import Sidebar from '../../shared/components/Sidebar';
import { StyledSection } from '../../shared/components/Section';
import StyledWrapper from '../../shared/components/Wrapper';

const Home = () => (
  <main>
    <Hero />
    <StyledSection>
      <StyledContainer>
        <StyledWrapper>
          <Sidebar />
          <ProductList />
        </StyledWrapper>
      </StyledContainer>
    </StyledSection>
  </main>
);

export default Home;
