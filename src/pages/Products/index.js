import React from 'react'
import { StyledSection, StyledSectionTitle, StyledSectionHeaderWrapper, StyledSectionHeader } from '../../shared/components/Section';
import Container from '../../shared/components/Container';
import ProductsGrid from '../../shared/components/ProductsGrid';

const Products = () => {
    return (
        <main>
            <StyledSection>
                <Container>
                    <StyledSectionHeader>
                        <StyledSectionHeaderWrapper>
                            <StyledSectionTitle>Products</StyledSectionTitle>
                        </StyledSectionHeaderWrapper>
                    </StyledSectionHeader>
                    <ProductsGrid />
                </Container>
            </StyledSection>
        </main>
    )
}

export default Products;
