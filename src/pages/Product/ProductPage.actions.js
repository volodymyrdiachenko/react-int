const API = 'http://localhost:3006';

export const PRODUCT_HAS_ERRORED = "PRODUCT_HAS_ERRORED";
export const PRODUCT_IS_LOADING = "PRODUCT_IS_LOADING";
export const PRODUCT_FETCH_DATA_SUCCESS = "PRODUCT_FETCH_DATA_SUCCESS";
export const PRODUCT_CLEAR_DATA = "PRODUCT_CLEAR_DATA";

export const productHasErrored = (bool) => {
    return {
        type: PRODUCT_HAS_ERRORED,
        hasErrored: bool
    };
}

export const productIsLoading = (bool) => {
    return {
        type: PRODUCT_IS_LOADING,
        isLoading: bool
    };
}

export const productFetchDataSuccess = (product) => {
    return {
        type: PRODUCT_FETCH_DATA_SUCCESS,
        payload: product
    };
}

export const productClearData = () => {
    return {
        type: PRODUCT_CLEAR_DATA,
        payload: null
    }
}

export const productFetchData = (keyword) => {
    return dispatch => {
        dispatch(productIsLoading(true));
        fetch(`${API}/products?id=${keyword}`)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                dispatch(productIsLoading(false));
                return response;
            })
            .then((response) => response.json())
            .then((product) => dispatch(productFetchDataSuccess(product)))
            .catch(() => dispatch(productHasErrored(true)));
    };
}   
