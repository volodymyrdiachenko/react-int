import {
  PRODUCT_HAS_ERRORED,
  PRODUCT_IS_LOADING,
  PRODUCT_FETCH_DATA_SUCCESS,
  PRODUCT_CLEAR_DATA
} from './ProductPage.actions';

export const productHasErrored = (state = false, action) => {
  switch (action.type) {
    case PRODUCT_HAS_ERRORED:
      return action.hasErrored;
    default:
      return state;
  }
}
export const productIsLoading = (state = false, action) => {
  switch (action.type) {
    case PRODUCT_IS_LOADING:
      return action.isLoading;
    default:
      return state;
  }
}
export const product = (state = [], action) => {
  switch (action.type) {
    case PRODUCT_FETCH_DATA_SUCCESS:
      return {
        ...state,
        product: action.payload
      }
    case PRODUCT_CLEAR_DATA:
      return {
        ...state,
        product: action.payload
      }
    default:
      return state;
  }
}