import React, { Component } from 'react'
import Product from '../../shared/components/Product';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { productFetchData, productClearData } from "./ProductPage.actions";
import StyledContainer from '../../shared/components/Container';
import { StyledSection } from '../../shared/components/Section';
import Loader from '../../shared/components/Loader';

class ProductPageStyledContainer extends Component {
    componentDidMount() {
        this.props.productFetchData(this.props.match.params.id)
    }

    componentWillUnmount() {
        this.props.productClearData();
    }

    render() {
        return (
            <StyledSection>
                <StyledContainer>
                    {this.props.data.product ? (
                        <Product product={this.props.data.product} />
                    ) : (<Loader />)}
                </StyledContainer>
            </StyledSection>
        );
    }
}

const mapStateToProps = state => {
    return {
        data: state.product
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            productFetchData,
            productClearData
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductPageStyledContainer);
