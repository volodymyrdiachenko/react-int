import React from "react";
import StyledContainer from '../../shared/components/Container';

const About = () => (
  <main>
    <section>
      <StyledContainer>
        <h2>About</h2>
      </StyledContainer>
    </section>
  </main>
);

export default About; 
