import React, { Fragment } from "react";
import { GlobalStyles } from "./shared/components/GlobalStyles";
import Header from "./shared/components/Header";
import Footer from "./shared/components/Footer";
import Routes from "./Routes";

const App = () => {
  return (
    <Fragment>
      <GlobalStyles color="#333" />
      <Header />
      <Routes />
      <Footer />
    </Fragment>
  );
}

export default App;
