import styled from "styled-components";
import { gridColumn } from "neat-components";

export const StyledFooter = styled.footer`
  padding: 100px 0;
  background-color: #ccc;
`;

export const StyledFooterWrapper = styled.div`
  ${props => gridColumn(props.theme, 12)}; 
`;

export const StyledParagraph = styled.p`
  text-align: ${p => p.align};
`;