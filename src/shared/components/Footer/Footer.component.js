import React from "react";
import Container from '../Container';
import { StyledFooter, StyledFooterWrapper, StyledParagraph } from "./Footer.styled";

const Footer = () => (
  <StyledFooter>
    <Container>
      <StyledFooterWrapper>
        <h2>Footer</h2>
        <StyledParagraph align="center">The footer goes here</StyledParagraph>
      </StyledFooterWrapper>
    </Container>
  </StyledFooter>
);

export default Footer;
