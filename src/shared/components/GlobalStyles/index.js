import { createGlobalStyle } from "styled-components";

export const GlobalStyles = createGlobalStyle`
    :root {
        font-size: 10px;    
    }

    html {
        box-sizing: border-box; 
    }

    *,
    *::before,
    *::after {
        box-sizing: inherit;
    }

    body {
        background: ${p => p.backgroundColor};
        font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
        color: ${p => p.color};
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        text-rendering: optimizeLegibility;
        font-size: 1.6rem;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    p {
        font-weight: normal;
        margin: 0;
    }

    ul {
        margin: 0;
        padding: 0;

        li {
            list-style-type: none;
        }
    }

    img {
        max-width: 100%;
        height: auto;
        display: block;
    }
`;