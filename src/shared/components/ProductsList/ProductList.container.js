import React, { Component } from 'react'
import ProductsList from './ProductList.component';

class ProductListContainer extends Component {
    state = {
        data: []
    }

    getData = () => {
        const url = 'http://localhost:3006/products';

        fetch(url)
            .then(response => response.json())
            .then(data => this.setState({ data }))
            .catch(error => console.log(error))
    }

    componentDidMount() {
        this.getData();
    }

    render() {
        const { data } = this.state;
        return (
            <ProductsList list={data} />
        );
    }
}

export default ProductListContainer;
