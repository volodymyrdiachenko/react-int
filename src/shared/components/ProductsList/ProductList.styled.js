import styled from 'styled-components';
import { gridColumn, gridMedia } from 'neat-components';
import { CustomGrid, GridMobile } from '../Grid';

export const StyledProductsList = styled.div`
    ${gridColumn(CustomGrid, 8)};
    ${gridMedia(GridMobile, [ 
    {
        ...gridColumn(GridMobile, 6)
    }
])};
`;