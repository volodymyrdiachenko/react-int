import React from 'react'
import { Link } from 'react-router-dom';
import { StyledProductsList } from './ProductList.styled';

const ProductsList = (props) => {
    return (
        <StyledProductsList>
            <h2>Products</h2>
            <ul>
                {props.list && props.list.map(item => (
                    <li key={item.id}>
                        <Link to={`products/${item.id}`}>
                            <img src={`images/${item.image}`} alt="imgs" />
                            {item.title}
                        </Link>
                    </li>
                ))}
            </ul>
        </StyledProductsList>
    );
}
export default ProductsList;