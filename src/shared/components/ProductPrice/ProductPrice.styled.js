import styled from 'styled-components';

const StyledProductPrice = styled.div`
    font-weight: bold;
    margin-top: 20px;
`;

export default StyledProductPrice