import React from 'react';
import StyledProductPrice from "./ProductPrice.styled";

const ProductPrice = (props) => (
    <StyledProductPrice>
        Price: {props.price}
    </StyledProductPrice>
);

export default ProductPrice;