import styled from "styled-components";
import { gridContainer } from "neat-components";

const StyledContainer = styled.div`
  ${gridContainer()};
  
  max-width: 1220px;
  margin-left: auto;
  margin-right: auto;
`;

export default StyledContainer;