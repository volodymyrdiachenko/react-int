import styled, { css } from 'styled-components';
import { gridColumn, gridMedia, gridContainer } from "neat-components";
import { CustomGrid, GridMobile } from '../Grid';
import media from '../Media';

export const StyledSection = styled.section`
  margin: 98px 0;
  padding: 100px 0;

  ${media.mobile` 
    margin: 46px 0;
    padding: 50px 0;
  `} 
`;

export const StyledSectionHeader = styled.div`
  ${gridContainer()}
`;

export const StyledSectionHeaderWrapper = styled.div`
  ${gridColumn(CustomGrid, 8)};
    ${gridMedia(GridMobile, [
  {
    ...gridColumn(GridMobile, 6)
  }
])}

${props =>
    props.fullwidth &&
    css`
      ${gridColumn(CustomGrid, 12)};
    `};

`;

export const StyledSectionTitle = styled.h2`
  margin-bottom: 20px;  
`;
