import styled from 'styled-components';
import { gridColumn, gridMedia, gridContainer } from 'neat-components';
import { CustomGrid, GridMobile } from '../Grid';

export const StyledProduct = styled.div`
    ${gridContainer()};
    display: flex;
    align-items: center;
`;

export const StyledProductWrapper = styled.div`
    padding: 16px;
    border: 1px solid;
    
    ${gridColumn(CustomGrid, 12)};
    ${gridMedia(GridMobile, [
    {
        ...gridColumn(GridMobile, 6)
    }
])};
`;

export const StyledProductEmbed = styled.div`
    ${gridColumn(CustomGrid, 6)};
    ${gridMedia(GridMobile, [
    {
        ...gridColumn(GridMobile, 6)
    }
])};
`;

export const StyledProductContent = styled.div`
    ${gridColumn(CustomGrid, 6)};
    ${gridMedia(GridMobile, [
    {
        ...gridColumn(GridMobile, 6)
    }
])};

    margin-top: 20px;
`;