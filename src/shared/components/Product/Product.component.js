import React from 'react';
import { StyledProductWrapper, StyledProduct, StyledProductEmbed, StyledProductContent } from "./Product.styled";
import ProductImage from "../ProductImage";
import ProductDescription from '../ProductDescription/ProductDescription.component';
import ProductPrice from '../ProductPrice/ProductPrice.component';

const Product = (props) => {
    let product = props.product[0];
    return (
        <StyledProduct>
            <StyledProductWrapper>
                <StyledProductEmbed>
                    <ProductImage image={product.imageFull} />
                </StyledProductEmbed>
                <StyledProductContent>
                    <ProductDescription desc={product.desc} />
                    <ProductPrice price={product.price} />
                </StyledProductContent>
            </StyledProductWrapper>
        </StyledProduct>
    )
}

export default Product;