import React from "react";
import { ThemeProvider } from "styled-components";
import Neat from "neat-components";
import Wrapper from "../Wrapper";
import { Column } from "./ProductsGrid.styles";

const ProductsGrid = () => {
    return (
        <ThemeProvider key="provider" theme={Neat()}>
            <Wrapper>
                <Column>Product 1</Column>
                <Column>Product 2</Column>
                <Column>Product 3</Column>
                <Column>Product 4</Column>
                <Column>Product 5</Column>
                <Column>Product 6</Column>
            </Wrapper>
        </ThemeProvider>
    );
}

export default ProductsGrid