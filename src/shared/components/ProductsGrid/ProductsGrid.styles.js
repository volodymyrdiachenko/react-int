import { CustomGrid, GridMobile } from '../Grid';
import { gridColumn, gridMedia } from "neat-components";
import styled from "styled-components";

export const Column = styled.div`
  ${gridColumn(CustomGrid, 3)};
  ${gridMedia(GridMobile, [
  {
    ...gridColumn(GridMobile, 6)
  }
])};

  margin-bottom: 24px;
  padding: 20px;
  background-color: magenta;
`;