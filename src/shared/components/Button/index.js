import styled, { css } from "styled-components";

const Button = styled.button`
  display: inline-block;
  border-radius: 3px;
  padding: 0.5rem 0;
  width: 11rem;
  text-align: center;
  background: transparent;
  text-decoration: none;
  color: white;
  font-size: 1rem;
  cursor: pointer;
  border: 2px solid white;

  :hover {
    opacity: 0.8;
    text-decoration: inherit;
  }

  ${props =>
    props.primary &&
    css`
      background: palevioletred;
      color: white;
    `};

  &:hover {
    ${props =>
    props.primary &&
    css`
        background: palevioletred;
        color: white;
      `};
  }
`;


export default Button;