import styled from 'styled-components';

export const StyledProductImage = styled.figure`
    margin-bottom: 10px;
    
    img {
        margin: auto;
    }
`;