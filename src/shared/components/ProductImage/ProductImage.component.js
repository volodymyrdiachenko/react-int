import React from "react";
import { StyledProductImage } from "./ProductImage.styled";

const ProductImage = (props) => (
    <StyledProductImage>
        <img src={`../../images/${props.image}`} alt="Product" />
    </StyledProductImage>
);

export default ProductImage;

