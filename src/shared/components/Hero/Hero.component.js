import React from "react";
import { StyledHero } from './Hero.styled';

const Hero = () => (
  <StyledHero>
    <h1>Hero</h1>
  </StyledHero>
);

export default Hero;
