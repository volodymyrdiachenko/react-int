import styled from 'styled-components';
import { gridContainer } from 'neat-components';

const StyledWrapper = styled.div`
  ${gridContainer()};
`;

export default StyledWrapper;