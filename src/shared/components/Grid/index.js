import Neat from "neat-components";

export const CustomGrid = Neat({
    columns: 12,
    gutter: "24px"
});

export const GridMobile = Neat({
    columns: 6,
    gutter: "20px",
    media: "only screen and (max-width: 767px)"
});