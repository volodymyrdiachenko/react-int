import React from "react";
import Container from "../Container";
import { StyledMenu, StyledMenuItem, StyledLink } from "./Header.styled";

const Header = () => {
  return (
    <header>
      <Container>
        <nav>
          <StyledMenu>
            <StyledMenuItem>
              <StyledLink to="/">Home</StyledLink>
            </StyledMenuItem>
            <StyledMenuItem>
              <StyledLink to="/products">Products</StyledLink>
            </StyledMenuItem>
            <StyledMenuItem>
              <StyledLink to="/about">About</StyledLink>
            </StyledMenuItem>
          </StyledMenu>
        </nav>
      </Container>
    </header>
  );
};

export default Header;
