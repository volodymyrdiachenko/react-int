import styled from 'styled-components';
import { Link } from "react-router-dom"; 

export const StyledMenu = styled.ul`
  display: flex;
  align-items: center;
  justify-content: space-around;
  margin: 0;
  padding: 20px 0;
`;

export const StyledMenuItem = styled.li`
  list-style-type: none;
`;

export const StyledLink = styled(Link)`
  text-decoration: none;

  &:hover {
    text-decoration: underline;
  }
`;