import React from 'react';

const ProductDescription = (props) => (
    <div>{props.desc}</div>
);

export default ProductDescription;