import React from 'react';
import { StyledSidebar } from './Sidebar.styled';

const Sidebar = () => {
    return (
        <StyledSidebar>
            <h2>Sidebar</h2>
        </StyledSidebar>
    );
}

export default Sidebar; 