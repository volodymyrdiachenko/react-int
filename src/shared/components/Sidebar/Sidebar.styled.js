import styled from 'styled-components';
import { CustomGrid, GridMobile } from '../Grid';
import { gridColumn, gridMedia } from 'neat-components';
import media from '../Media';

export const StyledSidebar = styled.aside`
    background-color: red;
    ${gridColumn(CustomGrid, 4)};
    ${gridMedia(GridMobile, [
    {
        ...gridColumn(GridMobile, 6)
    }
])};
${media.mobile` 
    margin-bottom: 40px;
`};
`;